using System;

namespace HoodedCrow.Scenes.Editor
{
    [Serializable]
    public class SceneData
    {
        public string ID;
        public string SceneName;
        public string ScenePath;
        public string Tag;

        public SceneData(string id, string sceneName, string scenePath)
        {
            this.ID = id;

            this.SceneName = sceneName;
            this.ScenePath = scenePath;

            this.Tag = "";
        }
    }
}