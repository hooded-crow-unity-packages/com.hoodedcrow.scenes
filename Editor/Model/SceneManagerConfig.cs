using System.Collections.Generic;
using UnityEngine;

namespace HoodedCrow.Scenes.Editor
{
    [CreateAssetMenu(menuName = "Hooded Crow/Scenes/Config")]
    public class SceneManagerConfig : ScriptableObject
    {
        public string SceneFilePath = DEFAULT_PATH;
        
        public bool UsesNamespace = false;
        public string SceneFilesNamespace = DEFAULT_NAMESPACE;
        
        public List<SceneData> ScenesDataList = new List<SceneData>();
        
        public bool CreateTagBasedFiles = false;
        public List<string> TagsList = new List<string>();

        private const string DEFAULT_PATH = "Plugins/SceneManager/";
        private const string DEFAULT_NAMESPACE = "HoodedCrow.Scenes";
    }
}