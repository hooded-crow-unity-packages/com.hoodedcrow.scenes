using System.Text;

namespace HoodedCrow.Scenes.Editor
{
    public static class FieldNameHelper
    {
        public static string ParsePathToFieldName(string scenePath)
        {
            string fieldName = scenePath;
            fieldName = fieldName.Replace("ASSETS/", "");
            fieldName = fieldName.Replace(".UNITY", "");
            fieldName = fieldName.Replace(' ', '_');
            fieldName = fieldName.Replace('/', '-');
            fieldName = CleanUpSceneNameForFile(fieldName);
            return fieldName;
        }

        public static string ParsePathToWindowFieldName(string scenePath)
        {
            string fieldName = scenePath;
            fieldName = fieldName.Replace("ASSETS/", "");
            fieldName = fieldName.Replace(".UNITY", "");
            fieldName = fieldName.Replace(' ', '_');
            fieldName = CleanUpSceneNameForWindow(fieldName);
            return fieldName;
        }

        private static string CleanUpSceneNameForFile(string sceneName)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in sceneName)
            {
                if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == ' ' || c == '_')
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        private static string CleanUpSceneNameForWindow(string sceneName)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in sceneName)
            {
                if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == ' ' || c == '_' ||
                    c == '/')
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }
    }
}