namespace HoodedCrow.Scenes.Editor
{
    public static class SceneFileNameHelper
    {
        public static string GetSceneName(string scenePath)
        {
            string[] splitPath = scenePath.Split('/');
            string sceneName = splitPath[splitPath.Length - 1].Split('.')[0];
            return sceneName;
        }
        public static string GetSceneNameFromAssetPath(string path)
        {
            string [] pathSplit = path.Split('/');
            string fileFullName = pathSplit[pathSplit.Length - 1];
            string fileName = fileFullName.Split('.')[0];

            return fileName;
        }

    }
}