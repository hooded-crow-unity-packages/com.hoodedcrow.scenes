using UnityEditor;
using UnityEngine;

namespace HoodedCrow.Scenes.Editor
{
    public partial class SceneManager : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            SceneManagerConfig config = Resources.Load<SceneManagerConfig>(CONFIG_FILE_NAME);
            
            bool newScene = ProcessImportedAssets(importedAssets, config);
            bool movedScene = ProcessMovedAssets(movedAssets, config);
            bool sceneDeleted = ProcessDeletedAssets(deletedAssets, config);

            if (newScene || movedScene || sceneDeleted)
            {
                GenerateFile(config);
            }
        }


        private static bool ProcessImportedAssets(string[] importedAssets, SceneManagerConfig config)
        {
            bool listUpdated = false;
            foreach (string assetPath in importedAssets)
            {
                if (assetPath.Contains(SCENE_FILE_FORMAT))
                {
                    if (IsSceneInManager(config, assetPath))
                    {
                        continue;
                    }

                    string GUID = AssetDatabase.AssetPathToGUID(assetPath);
                    string sceneName = SceneFileNameHelper.GetSceneName(assetPath);
                    SceneData data = new SceneData(GUID, sceneName, assetPath);

                    config.ScenesDataList.Add(data);

                    if (config.TagsList.Contains(DEFAULT_SCENE_TAG) == false)
                    {
                        config.TagsList.Add(DEFAULT_SCENE_TAG);
                    }

                    listUpdated = true;
                }
            }

            return listUpdated;
        }
        private static bool ProcessMovedAssets(string [] movedAssets, SceneManagerConfig config)
        {
            bool listUpdated = false;
            foreach (string assetPath in movedAssets)
            {
                if (assetPath.Contains(SCENE_FILE_FORMAT))
                {
                    string GUID = AssetDatabase.AssetPathToGUID(assetPath);
                    string newName = SceneFileNameHelper.GetSceneNameFromAssetPath(assetPath);

                    SceneData sceneData = config.ScenesDataList.Find(s=>s.ID.Equals(GUID));
                    int index = config.ScenesDataList.IndexOf(sceneData);
                    sceneData.SceneName = newName;
                    sceneData.ScenePath = assetPath;

                    config.ScenesDataList[index] = sceneData;
                    listUpdated = true;
                }
            }

            return listUpdated;
        }
        private static bool ProcessDeletedAssets(string [] deletedAssets, SceneManagerConfig config)
        {
            bool listUpdated = false;
            bool sceneWasDeleted = false;
            foreach (string assetPath in deletedAssets)
            {
                if (assetPath.Contains(SCENE_FILE_FORMAT))
                {
                    sceneWasDeleted = true;
                    string GUID = AssetDatabase.AssetPathToGUID(assetPath);
                    SceneData sceneData = config.ScenesDataList.Find(s => s.ID.Equals(GUID));
                    config.ScenesDataList.Remove(sceneData);
                    listUpdated = true;
                }
            }
            /*if(sceneWasDeleted)
            {
                SceneManagerConfig.ValidateTagsList();
            }*/

            return listUpdated;
        }

        private static bool IsSceneInManager(SceneManagerConfig config, string assetPath)
        {
            if (config.ScenesDataList.Count == 0)
            {
                return false;
            }
            
            string GUID = AssetDatabase.AssetPathToGUID(assetPath);

            return config.ScenesDataList.Find(s=>s.ID.Equals(GUID))!=null;
        }
    }
}