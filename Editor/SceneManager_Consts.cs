namespace HoodedCrow.Scenes.Editor
{
    public partial class SceneManager
    {
        private const string CONFIG_FILE_NAME = "Scene Manager Config";
        private const string SCENE_FILE_FORMAT = ".unity";
        private const string DEFAULT_SCENE_TAG = "default";
        
        private const string FILE_FORMAT = "public class {0}ScenesList\n{{\n{1}\n}}";
        private const string NAMESPACE_FILE_FORMAT =
            "namespace {0}\n{{\n\tpublic class {1}SceneList\n\t{{\n{2}\n\t}}\n}}";
        private const string FIELD_FORMAT = "\tpublic const string SCENE_{0} = \"{1}\";\n";
        private const string NAMESPACE_FIELD_FORMAT = "\t\tpublic const string SC_{0} = \"{1}\";\n";

    }
}