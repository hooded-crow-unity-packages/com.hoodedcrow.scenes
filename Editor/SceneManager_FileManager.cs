using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace HoodedCrow.Scenes.Editor
{
    public partial class SceneManager
    {
        public static void GenerateFile(SceneManagerConfig config)
        {
            ValidateFolderStructure();

            if (config.CreateTagBasedFiles)
            {
                //jGenerateTagFiles(config);
            }
            else
            {
                GenerateGeneralFile(config);
            }

            EditorUtility.SetDirty(config);
            AssetDatabase.Refresh();
        }

        private static void ValidateFolderStructure()
        {
            SceneManagerConfig config = Resources.Load<SceneManagerConfig>(CONFIG_FILE_NAME);
            string[] pathSplit = config.SceneFilePath.Split('/');

            if (Directory.Exists(GetFullPathToFile(config)))
            {
                return;
            }

            string previousFolder = "Assets";

            for (int i = 0; i < pathSplit.Length; i++)
            {
                if (string.IsNullOrEmpty(pathSplit[i]))
                {
                    continue;
                }

                if (Directory.Exists($"{Application.dataPath}/{previousFolder}{pathSplit[i]}") == false)
                {
                    AssetDatabase.CreateFolder(previousFolder, pathSplit[i]);
                }

                previousFolder += $"/{pathSplit[i]}";
            }
        }

        private static string GetFullPathToFile(SceneManagerConfig config)
        {
            return $"{Application.dataPath}/{config.SceneFilePath}";
        }
        
        private static void GenerateGeneralFile(SceneManagerConfig config)
        {
            string path = $"{GetFullPathToFile(config)}/ScenesList.cs";

            if (File.Exists(path) == false)
            {
                File.Create(path).Close();
            }

            string content = CreateFileContent(config);
            File.WriteAllText(path, content);
        }
        private static string CreateFileContent(SceneManagerConfig config)
        {
            string content;

            string fields = GenerateFileFields(config);

            if (config.UsesNamespace)
            {
                content = string.Format(NAMESPACE_FILE_FORMAT, config.SceneFilesNamespace, string.Empty, fields);
            }
            else
            {
                content = string.Format(FILE_FORMAT, string.Empty, fields);
            }

            return content;
        }
        private static string GenerateFileFields(SceneManagerConfig config)
        {
            string fields = "";

            List<SceneData> filteredList =
                config.ScenesDataList.GroupBy(s => s.SceneName).Select(g => g.First()).ToList();

            foreach (SceneData data in filteredList)
            {
                string fieldName = FieldNameHelper.ParsePathToFieldName(data.ScenePath);
                string format = config.UsesNamespace ? NAMESPACE_FIELD_FORMAT : FIELD_FORMAT;
                string field = String.Format(format, fieldName, data.ScenePath);
                fields += field;
            }

            return fields;
        }
        

    }
}